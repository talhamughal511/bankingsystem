from django.db import models

# Create your models here.

class Client(models.Model):
    name = models.CharField(max_length=250)
    address = models.CharField(max_length=250)
    Phone = models.CharField(max_length=15)
    dob = models.DateField()
    email = models.EmailField()

    def __str__(self):
        return self.name
from django.urls import path
from .views import ShowBalance, DepositCash, WithdrawCash, TransCash

urlpatterns = [
    path('showbal/', ShowBalance.as_view()),
    path('cashdep/', DepositCash.as_view()),
    path('cashwid/', WithdrawCash.as_view()),
    path('transcash/', TransCash.as_view()),
]
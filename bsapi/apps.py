from django.apps import AppConfig


class BsapiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bsapi'

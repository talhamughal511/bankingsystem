from django.db import models
from datetime import datetime

# Create your models here.





class Account(models.Model):
    name = models.CharField(max_length=250)
    open_date = models.DateField()
    client = models.OneToOneField(Client, on_delete=models.CASCADE)
    balance = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Transaction(models.Model):
    account = models.ForeignKey(Client, on_delete=models.CASCADE)
    amount = models.PositiveIntegerField()
    date_of_trans = models.DateTimeField(default=datetime.now())

    def __str__(self):
        return self.account


class Withdraw(models.Model):
    amount = models.PositiveIntegerField()
    account = models.ForeignKey(Client, on_delete=models.CASCADE)


class Deposit(models.Model):
    amount = models.PositiveIntegerField()
    account = models.ForeignKey(Client, on_delete=models.CASCADE)

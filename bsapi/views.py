from django.db import transaction
from django.db.models import F
from .models import Client, Account, Transaction, Withdraw, Deposit
from .serializers import ClientSerializer, AccountSerializer, TransactionSerializer, \
    WithdrawSerializer, DepositSerializer
from rest_framework.response import Response
from rest_framework import status
from datetime import date, datetime
from rest_framework.views import APIView


# Create your views here.


class ShowBalance(APIView):

    @staticmethod
    def get(request):
        bal = Account.objects.filter(client=2).values('balance')
        return Response({'data': bal})


class DepositCash(APIView):

    @staticmethod
    def get(request):
        # dep = Deposit.objects.all()

        bal = Account.objects.filter(client=2).values('balance')
        return Response({'data': bal})
        # serializer = DepositSerializer(bal, many=True)
        # return Response(serializer.data)

    @staticmethod
    def post(request, *args, **kwargs):
        serializer = DepositSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            with transaction.atomic():
                Account.objects.filter(client=serializer.validated_data.get('account')).update(balance=F('balance') +
                                                                serializer.validated_data.get('amount', 0))
                serializer.save()
                return Response({'message': 'money added successfully'}, status=status.HTTP_201_CREATED)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class WithdrawCash(APIView):

    @staticmethod
    def get(request):
        # dep = Deposit.objects.all()

        bal = Account.objects.filter(client=2).values('balance')
        return Response({'data': bal})

    @staticmethod
    def post(request, *args, **kwargs):
        serializer = WithdrawSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            with transaction.atomic():
                cl = Account.objects.filter(client=serializer.validated_data.get('account')).values('balance')
                ci = cl[0].get('balance')
                if cl and ci >= serializer.validated_data.get('amount'):
                    Account.objects.filter(client=serializer.validated_data.get('account')).update(balance=F('balance') -
                                                                serializer.validated_data.get('amount', 0))
                    serializer.save()
                    return Response({'message': 'money withdrawl successful'}, status=status.HTTP_201_CREATED)
                else:
                    return Response({'message': 'insufficient balance'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)


class TransCash(APIView):

    @staticmethod
    def post(request, *args, **kwargs):
        serializer = TransactionSerializer(data=request.data)

        if serializer.is_valid(raise_exception=True):
            with transaction.atomic():
                cl = Account.objects.filter(client=2).values('balance')
                ci = cl[0].get('balance')
                if cl and ci >= serializer.validated_data.get('amount'):
                    Account.objects.filter(client=2).update(balance=F('balance') -
                                                                    serializer.validated_data.get('amount', 0))
                    Account.objects.filter(client=serializer.validated_data.get('account')).update(
                        balance=F('balance') + serializer.validated_data.get('amount', None))
                    serializer.save()
                    return Response({'message': 'Transfer Done successfully'}, status=status.HTTP_201_CREATED)
                else:
                    return Response({'message': 'insufficient balance'}, status=status.HTTP_400_BAD_REQUEST)
        return Response(status=status.HTTP_400_BAD_REQUEST)

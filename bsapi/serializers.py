from rest_framework import serializers
from .models import Client, Account, Transaction, Withdraw, Deposit


class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Client
        fields = '__all__'


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = '__all__'


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        # fields = '__all__'
        fields = ['amount', 'account']

class WithdrawSerializer(serializers.ModelSerializer):
    class Meta:
        model = Withdraw
        # fields = ['amount']
        fields = '__all__'


class DepositSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deposit
        fields = '__all__'
        # fields = ['amount']
